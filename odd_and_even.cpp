#include <iostream>



void OddorEvenNumbersPrint(const int X,int odd_or_even)
{
	for (int i = 0; i <= X; i++)
	{
		if (odd_or_even == 0)
		{
			if (i % 2 == 0)
			{
				std::cout << i << ',';
			}
		}
		else
		{
			if (i % 2 == 1)
			{
				std::cout << i << ',';
			}
		}
	}
}

int main()
{
	const int N = 10;

	for (int i = 0; i <= N; i++)
	{
		if (i%2==0)
		{
			std::cout << "Digit " << i << ' ' << "Is Even \n";
		}
	}

	OddorEvenNumbersPrint(1048, 1);
}